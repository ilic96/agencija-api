<b>Zadatak: </b><br/>
Korišćenjem radnih okvira ASP.NET Web API, Entity Framework i Bootstrap, JQuery<br/>
biblioteke i Partial View-a realizovati Web aplikaciju za vođenje evidencije o<br/>
agentima nekretnina i nekretninama. Aplikacija treba da obezbedi rad sa sledećim<br/>
entitetima:<br/><br/>
Agent:<br/>
● Id - Identifikator<br/>
● Ime i prezime - obavezna tekstualna vrednost sa najviše 50 karaktera<br/>
● Licenca - obavezna tekstualna vrednost sa tačno 4 karaktera<br/>
● Godina rođenja - celobrojna vrednost iz intervala (1950, 1995]<br/>
● Broj prodatih nekretnina - obavezna celobrojna vrednost veća od -1, a manja<br/>
ili jednaka 50.<br/><br/>
Nekretnina:<br/>
● Id - Identifikator<br/>
● Mesto - obavezna tekstualna vrednost sa najviše 40 karaktera<br/>
● Agencijska oznaka - obavezna tekstualna vrednost sa manje od 6 karaktera<br/>
● Godina izgradnje - celobrojna vrednost manja od 2019, a veća ili jednaka od
1900<br/>
● Kvadratura - obavezna decimalna vrednost veća od 2<br/>
● Cena - obavezna decimalna vrednost veća od 0 a manja ili jednaka 100 000<br/>
● Agent - veza sa instancom klase Agent (jedna nekretnina može imati samo
jednog agenta).<br/>
1) Pomoću radnih okvira ASP.NET Web API i Entity Framework implementirati
sledeći REST API:<br/>
a) GET api/agenti - preuzimanje svih agenata<br/>
b) GET api/agenti/{id} - preuzimanje agenata po zadatom id-u<br/>
c) GET api/ekstremi - prikaz agenta sa najvećim brojem prodatih<br/>
nekretnina i agenta sa najmanjim brojem prodatih nekretnina, sortirano
po broju prodatih nekretnina opadajuće<br/>
d) GET api/najmladji - preuzimanje svih agenata, prvo se prikazuju
najmlađi<br/>
e) GET api/nekretnine - preuzimanje svih nekretnina, sortiranih po ceni
opadajuće<br/>
f) GET api/nekretnine/{id} - preuzimanje nekretnine po zadatom id-u<br/>
g) GET api/nekretnine/?napravljeno={vrednost} - preuzimanje svih<br/>
nekretnina čija je godina izgradnje nakon prosleđene godine, sortirano
po godini izgradnje rastuće<br/>

h) POST api/nekretnine - dodavanje nove nekretnine<br/>
i) PUT api/nekretnine/{id} - izmena postojeće nekretnine<br/>
j) DELETE api/nekretnine/{id} - brisanje postojeće nekretnine<br/>
k) POST api/pretraga - preuzimanje nekretnina sa kvadraturom između<br/>
dve unete vrednosti [mini, maksi), sortiranih po kvadraturi rastuće<br/>
l) Registracija i prijava korisnika.<br/>
2) Jedinično testirati sledeće funkcionalnosti:<br/>
a) Funkcionalnost 1)f) sa jediničnim testom kada akcija vraća status kod 200 i
objekat<br/>
b) Funkcionalnost 1)j) sa jediničnim testom kada akcija vraća status kod 404<br/>
c) Funkcionalnost 1)i) sa jediničnim testom kada akcija vraća status kod 400<br/>
d) Funkcionalnost 1)k) sa jediničnim testom kada akcija vraća kolekciju objekata.<br/>