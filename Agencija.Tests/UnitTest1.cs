﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Agencija.Controllers;
using Agencija.Interfaces;
using Agencija.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Agencija.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsNekretninaWithSameId()
        {
            // Arrange
            var mockRepository = new Mock<INekretninaRepository>();
            mockRepository.Setup(x => x.GetById(42)).Returns(new Models.Nekretnina { Id = 42 });

            var controller = new NekretnineController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetNekretnina(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Nekretnina>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.Id);
        }
        [TestMethod]
        public void DeleteReturnsNotFount()
        {
            // Arrange
            var mockRepository = new Mock<INekretninaRepository>();
            var controller = new NekretnineController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.DeleteNekretnina(20);

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));
        }
        [TestMethod]
        public void PutReturnsBadRequest()
        {
            // Arrange
            var mockRespository = new Mock<INekretninaRepository>();
            var controller = new NekretnineController(mockRespository.Object);

            // Act
            IHttpActionResult actionResult = controller.PutNekretnina(1, new Nekretnina { Id = 2, Mesto = "Novi Sad", Cena = 20000m, Godina_izgradnje = 2000, Kvadratura = 40, Oznaka = "oznaka", AgentId = 1 });

            // Assert
            Assert.IsInstanceOfType(actionResult, typeof(BadRequestResult));
        }
        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            // Arrange
            List<Nekretnina> nekretnine = new List<Nekretnina>();
            nekretnine.Add(new Nekretnina { Id = 1, Cena = 20000m, Mesto = "Novi Pazar", Oznaka = "o1", Kvadratura = 80, Godina_izgradnje = 2005, AgentId = 1 });
            nekretnine.Add(new Nekretnina { Id = 2, Cena = 30000m, Mesto = "Beograd", Oznaka = "o2", Kvadratura = 70, Godina_izgradnje = 2006, AgentId = 2 });

            var mockRepository = new Mock<INekretninaRepository>();
            mockRepository.Setup(x => x.GetAll()).Returns(nekretnine.AsEnumerable());
            var controller = new NekretnineController(mockRepository.Object);

            // Act
            IEnumerable<Nekretnina> rezultat = controller.GetNekretnine();

            // Assert
            Assert.IsNotNull(rezultat);
            Assert.AreEqual(nekretnine.Count, rezultat.ToList().Count);
            Assert.AreEqual(nekretnine.ElementAt(0), rezultat.ElementAt(0));
            Assert.AreEqual(nekretnine.ElementAt(1), rezultat.ElementAt(1));

        }
    }
}
