﻿using Agencija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agencija.Interfaces
{
    public interface IAgentiRepository
    {
        IEnumerable<Agent> GetAll();
        IEnumerable<Agent> GetEkstremni();
        IEnumerable<Agent> GetNajmladji();
        Agent GetById(int id);
        
    }
}
