﻿using Agencija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Agencija.Interfaces
{
    public interface INekretninaRepository
    {
        IEnumerable<Nekretnina> GetAll();
        IEnumerable<Nekretnina> GetNapravljeno(int napravljeno);
        IEnumerable<Nekretnina> Pretraga(int mini, int maksi);
        Nekretnina GetById(int id);
        void Add(Nekretnina nekretnina);
        void Update(Nekretnina nekretnina);
        void Delete(Nekretnina nekretnina);
    }
}
