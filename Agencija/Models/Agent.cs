﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Agencija.Models
{
    public class Agent
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [StringLength(50)]
        public string Ime_Prezime { get; set; }
        [Required]
        [StringLength(4)]
        public string Licenca { get; set; }
        [Range(1950, 1996)]
        public int Godina_rodjenja { get; set; }
        [Required]
        [Range(0, 51)]
        public int BrProdatih_nekretnina { get; set; }
    }
}