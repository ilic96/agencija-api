namespace Agencija.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Agencija.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Agencija.Models.ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            context.Agenti.AddOrUpdate
                (
                    new Models.Agent() { Id = 1, Ime_Prezime = "Pera Peric", Licenca = "Lic1", Godina_rodjenja = 1960, BrProdatih_nekretnina = 15},
                    new Models.Agent() { Id = 2, Ime_Prezime = "Mika Mikic", Licenca = "Lic2", Godina_rodjenja = 1960, BrProdatih_nekretnina = 10}, 
                    new Models.Agent() { Id = 3, Ime_Prezime = "Zika Zikic", Licenca = "Lic3", Godina_rodjenja = 1980, BrProdatih_nekretnina = 5}
                );
            context.SaveChanges();
            context.Nekretnine.AddOrUpdate
                (
                    new Models.Nekretnina() { Id = 1, Mesto = "Novi Sad", Oznaka = "Nek01", Godina_izgradnje = 1974, Kvadratura = 50, Cena = 40000m, AgentId = 1},
                    new Models.Nekretnina() { Id = 2, Mesto = "Beograd", Oznaka = "Nek02", Godina_izgradnje = 1990, Kvadratura = 60, Cena = 50000m, AgentId = 2},
                    new Models.Nekretnina() { Id = 3, Mesto = "Subotica", Oznaka = "Nek03", Godina_izgradnje = 1995, Kvadratura = 55, Cena = 45000m, AgentId = 3},
                    new Models.Nekretnina() { Id = 4, Mesto = "Zrenjanin", Oznaka = "Nek04", Godina_izgradnje = 2010, Kvadratura = 70, Cena = 60000m, AgentId = 1}             
                );
            context.SaveChanges();
        }
    }
}
