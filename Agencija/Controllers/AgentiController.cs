﻿using Agencija.Interfaces;
using Agencija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Agencija.Controllers
{
    public class AgentiController : ApiController
    {
        private IAgentiRepository _repository { get; set; }
        public AgentiController(IAgentiRepository repository)
        {
            _repository = repository;
        }
        //GET api/agenti
        [ResponseType(typeof(Agent))]
        public IEnumerable<Agent> GetAgenti()
        {
            return _repository.GetAll();
        }
        //GET api/agenti/1
        [ResponseType(typeof(Agent))]
        public IHttpActionResult GetAgent(int id)
        {
            var agent = _repository.GetById(id);
            if(agent == null)
            {
                return NotFound();
            }
            return Ok(agent);
        }
        //GET api/ekstremni
        [ResponseType(typeof(Agent))]
        [Route("api/ekstremni")]
        public IEnumerable<Agent> GetEkstremni()
        {
            return _repository.GetEkstremni();
        }
        //GET api/najmladji
        [ResponseType(typeof(Agent))]
        [Route("api/najmladji")]
        public IEnumerable<Agent> GetNajmladji()
        {
            return _repository.GetNajmladji();
        }

    }
}
