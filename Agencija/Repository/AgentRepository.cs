﻿using Agencija.Interfaces;
using Agencija.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Agencija.Repository
{
    public class AgentRepository : IDisposable, IAgentiRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Agent> GetAll()
        {
            return db.Agenti;
        }

        public IEnumerable<Agent> GetEkstremni()
        {
            return db.Agenti.OrderByDescending(x => x.BrProdatih_nekretnina);
        }

        public IEnumerable<Agent> GetNajmladji()
        {
            return db.Agenti.OrderByDescending(x => x.Godina_rodjenja);
        }

        public Agent GetById(int id)
        {
            return db.Agenti.FirstOrDefault(x => x.Id == id);
        }
    }
}