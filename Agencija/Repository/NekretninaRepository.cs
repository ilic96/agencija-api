﻿using Agencija.Interfaces;
using Agencija.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace Agencija.Repository
{
    public class NekretninaRepository : IDisposable, INekretninaRepository
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public IEnumerable<Nekretnina> GetAll()
        {
            return db.Nekretnine.Include(x => x.Agent);
        }

        public IEnumerable<Nekretnina> GetNapravljeno(int napravljeno)
        {
            IEnumerable<Nekretnina> filter = from x in db.Nekretnine.Include(a => a.Agent).OrderBy(g => g.Godina_izgradnje)
                                             where napravljeno < x.Godina_izgradnje
                                             select x;
            return filter;
        }

        public IEnumerable<Nekretnina> Pretraga(int mini, int maksi)
        {
            IEnumerable<Nekretnina> filter = from x in db.Nekretnine.Include(a => a.Agent).OrderBy(k => k.Kvadratura)
                                             where mini < x.Kvadratura && maksi > x.Kvadratura
                                             select x;
            return filter;
        }

        public Nekretnina GetById(int id)
        {
            return db.Nekretnine.FirstOrDefault(x => x.Id == id);
        }

        public void Add(Nekretnina nekretnina)
        {
            db.Nekretnine.Add(nekretnina);
            db.SaveChanges();
        }

        public void Update(Nekretnina nekretnina)
        {
            db.Entry(nekretnina).State = System.Data.Entity.EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }

        public void Delete(Nekretnina nekretnina)
        {
            db.Nekretnine.Remove(nekretnina);
            db.SaveChanges();
        }
    }
}